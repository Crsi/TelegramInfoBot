#!/usr/bin/env python3

import telegram.ext

from config import config


def setup():
    """
    Setup the Telegram bot environment and start the polling loop
    """

    updater = telegram.ext.Updater(config["token"], use_context = True)
    add = updater.dispatcher.add_handler

    add(telegram.ext.CommandHandler("id", getID))
    add(telegram.ext.CommandHandler("account", getAccount))
    add(telegram.ext.CommandHandler("chat", getChat))
    add(telegram.ext.CommandHandler("bot", getBot))

    updater.start_polling(bootstrap_retries = 1, timeout = 1)
    updater.idle()


if __name__ == "__main__":
    setup()
