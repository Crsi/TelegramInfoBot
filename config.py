#!/usr/bin/env python3

"""
Configuration module

You should be able to use the configuration stored in the
specified JSON file from any module by importing the
config dictionary like this:

>>> from config import config

Now you're able to access the contents of the JSON
storage file using the config variable in your code.
"""

import sys
import json


_CONFIG_PATH = "config.json"


try:
    with open(_CONFIG_PATH) as f:
        config = json.load(f)
except FileNotFoundError as exc:
    print(exc, file = sys.stderr)
    exit(1)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
