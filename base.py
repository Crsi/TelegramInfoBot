#!/usr/bin/env python3

import time
import typing
import telegram
import traceback
import telegram.ext


CommandFeature = typing.Callable[
    [telegram.Update, telegram.ext.CallbackContext],
    typing.Any
]


def failsafe(func: CommandFeature) -> CommandFeature:
    def _exec(update: telegram.Update, context: telegram.ext.CallbackContext):
        try:
            return func(update, context)
        except Exception as exc:
            print("{1} {0} {1}\n{2}".format(
                time.asctime(),
                "=" * 20,
                traceback.format_exc()
            ))
    _exec.__qualname__ = func.__qualname__
    return _exec


class Track:
    @staticmethod
    def getUpdateTypes() -> typing.List[str]:
        """
        Return a list of all currently supported telegram.Update types
        """

        return [
            "message",
            "edited_message",
            "channel_post",
            "edited_channel_post",
            "inline_query",
            "chosen_inline_result",
            "callback_query",
            "shipping_query",
            "pre_checkout_query",
            "poll",
            "poll_answer"
        ]

    @staticmethod
    def determine(update: telegram.Update, fallback: str = "update") -> str:
        """
        Determine the type of update as string
        """

        for t in Track.getUpdateTypes():
            if hasattr(update, t):
                return t
        return fallback

    def __init__(self, user: str, event: str):
        self._user = user
        self._event = event

    def __call__(self, func: CommandFeature) -> CommandFeature:
        def _exec(
                update: telegram.Update,
                context: telegram.ext.CallbackContext
        ) -> typing.Any:

            updateType = Track.determine(update)

            if self._user != "":
                if self._user not in context.user_data:
                    context.user_data[self._user] = {}
                if updateType not in context.user_data[self._user]:
                    context.user_data[self._user][command] = 0
                context.user_data[self._user][command] += 1

            if self._event != "":
                if self._event not in context.bot_data:
                    context.bot_data[self._event] = {}
                if updateType not in context.bot_data[self._event]:
                    context.bot_data[self._event][updateType] = 0
                context.bot_data[self._event][updateType] += 1

            return func(update, context)

        _exec.__qualname__ = func.__qualname__
        return _exec
